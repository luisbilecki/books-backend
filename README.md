# Books Backend

![CircleCI](https://img.shields.io/circleci/build/bitbucket/luisbilecki/books-backend)

This repository contains a CRUD of Books with JWT Authentication. This backend uses Node.JS, Express, Sequelize, and Postgres as RDBMS.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/8cbe4d28c8852b3a793b)

## Requirements

```
Node.js >= 10.x
Postgres
```

---

## How to run?

1. Run `yarn install` to install required dependencies for this project;

2. Create .env file using the provided template (.env.example);

3. In the project folder, generate a pair of public and private keys. The pair will be used to sign and verify the JWT token.

```bash
   openssl genrsa -out secret.key 512
   openssl rsa -in secret.key -outform PEM -pubout -out public.key
```

- Copy generated keys to .env:

```bash
  JWT_PRIVATE_KEY=-----BEGIN RSA PRIVATE KEY-----\nMIIBOwIB ...
  JWT_PUBLIC_KEY=-----BEGIN PUBLIC KEY-----\nMFwwD ...
```

- To convert the key to string with '\n', you may use this script:

```javascript
> const fs = require('fs');
> const key = fs.readFileSync('./<KEY NAME>.key', 'utf8');
> console.log(JSON.stringify(x));
`-----BEGIN RSA PRIVATE KEY-----\n...`
```

4. Run API using:

```bash
    yarn start

    or

    node src/index.js
```

5. Run tests using the following command:

```bash
    yarn test
```

6. Linting can be checked using:

```bash
    yarn lint
```

## Rest API endpoints

The REST API endpoints is described below.

| HTTP Method | HTTP Path            | Description       | Authentication needed? |
| ----------- | -------------------- | ----------------- | ---------------------- |
| POST        | /auth/signin         | Sign in a user    | NO                     |
| POST        | /auth/signup         | Register a user   | NO                     |
| POST        | /auth/validate-token | Verify JWT Token  | NO                     |
| GET         | /books               | List of books     | Yes (Bearer)           |
| GET         | /books/:isbn         | Show book details | Yes (Bearer)           |
| POST        | /books               | Create new book   | Yes (Bearer)           |
| PUT         | /books/:isbn         | Update book       | Yes (Bearer)           |
| DELETE      | /books/:isbn         | Delete book       | Yes (Bearer)           |

### Sign in

Used to collect a token for a registered user.

**URL** : `/auth/signin`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "username": "[valid email address]",
  "password": "[password in plain text]"
}
```

**Data example**

```json
{
  "username": "luis@luis.com",
  "password": "abcd123456"
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "success": true,
  "token": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
}
```

#### Error Response

**Condition** : If 'username' and 'password' combination is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "msg": "Invalid value",
        "param": "email",
        "location": "body"
      },
      {
        "msg": "Invalid value",
        "param": "password",
        "location": "body"
      }
    ]
  },
  "path": "/signin",
  "timestamp": 1579384336042
}
```

**Condition** : Invalid password.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "message": "Wrong password!",
  "path": "/auth/signin",
  "timestamp": 1579384385873
}
```

**Condition** : Email not found

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "status": 404,
  "error": "Not Found",
  "path": "/auth/signin",
  "timestamp": 1579384427654
}
```

---

### Sign up

Used to register a user.

**URL** : `/auth/signup`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "name": "[text]", // optional
  "username": "[valid email address]", // required
  "password": "[password in plain text]" // required
}
```

**Data example**

```json
{
  "name": "Luis",
  "username": "luis@luis.com",
  "password": "abcd123456"
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "success": true
}
```

#### Error Response

**Condition** : If 'username' and 'password' combination is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "msg": "Invalid value",
        "param": "email",
        "location": "body"
      },
      ...
    ]
  },
  "path": "/auth/signin",
  "timestamp": 1579384336042
}
```

---

### Validate user token

Used to check if a token is not expired or invalid.

**URL** : `/auth/validate-token`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

```json
{
  "token": "[your token]" // required
}
```

**Data example**

```json
{
  "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9"
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "status": "valid"
}
```

#### Error Response

**Condition** : If 'token' is empty.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "value": "",
        "msg": "Invalid value",
        "param": "token",
        "location": "body"
      }
    ]
  },
  "path": "/auth/validate-token",
  "timestamp": 1579384646592
}
```

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/auth/validate-token",
  "timestamp": 1579384688480
}
```

---

### List all books

Used to return a list of books

**URL** : `/books`

**Method** : `GET`

**Auth required** : YES (Bearer)

**Data constraints - Query**

```json
{
  "page": [number], // optional,
  "per_page": [number]] // optional,
}
```

**Data example - Query**

```json
{
  "page": 1 // optional,
  "per_page": 25 // optional,
}
```

/books?page=1&per_page=25

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "data": [
        {
            "isbn": "1933988452",
            "title": "Website Owner's Manual",
            "authors": "Paul A. Boag",
            "short_description": null,
            "publisher": null,
            "language": null,
            "pages": 296,
            "created_at": "2020-01-18T17:24:03.602Z",
            "updated_at": "2020-01-18T17:24:03.602Z"
        },
        ...
    ],
    "current_page": 1,
    "total_count": 388,
    "per_page": 5
}
```

#### Error Response

**Condition** : If pagination params are wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "value": "0",
        "msg": "Invalid value",
        "param": "page",
        "location": "query"
      }
    ]
  },
  "path": "/",
  "timestamp": 1579384789922
}
```

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/books",
  "timestamp": 1579384800324
}
```

---

### Get book data

Used to return the book details

**URL** : `/books/:isbn`

**Method** : `GET`

**Auth required** : YES (Bearer)

**Data constraints - Query**

```json
{
  "isbn": [string]
}
```

**Data example - Query**

```json
{
  "isbn": "1933988452"
}
```

/books/1933988452

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "isbn": "1933988452",
  "title": "Website Owner's Manual",
  "authors": "Paul A. Boag",
  "short_description": null,
  "publisher": null,
  "language": null,
  "pages": 296,
  "created_at": "2020-01-18T17:24:03.602Z",
  "updated_at": "2020-01-18T17:24:03.602Z"
}
```

#### Error Response

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/books/123",
  "timestamp": 1579384800324
}
```

**Condition** : If 'isbn' is not found.

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "status": 404,
  "error": "Not Found",
  "path": "/books/123",
  "timestamp": 1579384800324
}
```

---

### Create a book

Used to register a new book

**URL** : `/books`

**Method** : `POST`

**Auth required** : YES (Bearer)

**Data constraints**

```json
{
  "isbn": [text], // required
  "title": [text], // required
  "authors": [text], // required
  "shortDescription": [text], // optional
  "pages": [number], // optional
  "publisher": [text], // optional
  "language": [text] // optional
}
```

**Data example**

```json
{
  "isbn": "12345678",
  "title": "New Book",
  "authors": "Luis F. Bilecki"
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "success": true
}
```

#### Error Response

**Condition** : If one of required attributes are invalid.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "msg": "Invalid value",
        "param": "isbn",
        "location": "body"
      },
      {
        "msg": "Invalid value",
        "param": "title",
        "location": "body"
      },
      {
        "msg": "Invalid value",
        "param": "authors",
        "location": "body"
      }
    ]
  },
  "path": "/books",
  "timestamp": 1579385339534
}
```

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/books",
  "timestamp": 1579384800324
}
```

---

### Update book

**URL** : `/books/:isbn`

**Method** : `PUT`

**Auth required** : YES (Bearer)

**Data constraints**

```json
{
  "title": [text], // optional
  "authors": [text], // optional
  "shortDescription": [text], // optional
  "pages": [number], // optional
  "publisher": [text], // optional
  "language": [text] // optional
}
```

**Data example**

```json
{
  "title": "Updated title comes here"
}
```

#### Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "success": true
}
```

#### Error Response

**Condition** : If one of required attributes are invalid.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "status": 400,
  "error": "Bad request",
  "message": {
    "errors": [
      {
        "msg": "Invalid value",
        "param": "title",
        "location": "body"
      }
    ]
  },
  "path": "/",
  "timestamp": 1579385339534
}
```

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/books/195555",
  "timestamp": 1579384800324
}
```

**Condition** : If isbn is not found.

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "status": 404,
  "error": "Not Found",
  "path": "/books/195412515",
  "timestamp": 1579385479239
}
```

---

### Delete book

Used to remove a book from DB

**URL** : `/books/:isbn`

**Method** : `DELETE`

**Auth required** : YES (Bearer)

**Data constraints - Query**

```json
{
  "isbn": [string]
}
```

**Data example - Query**

```json
{
  "isbn": "1933988452"
}
```

/books/1933988452

#### Success Response

**Code** : `204 NO CONTENT`

**Content example**

```json

```

#### Error Response

**Condition** : If 'token' is invalid.

**Code** : `401 UNAUTHORIZED`

**Content** :

```json
{
  "status": 401,
  "error": "Unauthorized",
  "path": "/books/193239458",
  "timestamp": 1579384800324
}
```

**Condition** : If 'isbn' is not found.

**Code** : `404 NOT FOUND`

**Content** :

```json
{
  "status": 404,
  "error": "Not Found",
  "path": "/books/193239458",
  "timestamp": 1579385142333
}
```

## Notes

- This API uses JWT to authentication and this token is generated using the `jsonwebtoken` package (https://github.com/auth0/node-jsonwebtoken). Therefore, an auth service (e.g. Auth0, AWS Cognito) may be used to handle user authentication and management.
