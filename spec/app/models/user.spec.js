const faker = require('faker');

const { User } = require('../../../app/models');

describe('User Model', () => {
  const password = '12345678';
  let defaultUser = null;

  beforeAll(async () => {
    defaultUser = await User.create({
      name: faker.name.findName(),
      email: faker.internet.email(),
      password,
    });
  });

  it('should hash user password', async () => {
    const user = await User.create({
      name: faker.name.findName(),
      email: faker.internet.email(),
      password,
    });

    expect(user.password).not.toEqual(password);
  });

  it('should return true when password matches', async () => {
    expect(defaultUser.comparePassword(password)).toBeTruthy();
  });

  it('should return false when password does not match', async () => {
    expect(defaultUser.comparePassword('luis')).toBeFalsy();
  });
});
