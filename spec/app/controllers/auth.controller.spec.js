const request = require('supertest');
const faker = require('faker');

const app = require('../../../app/index');

const { User } = require('../../../app/models');

const token = require('../../fixtures/token');

describe('Auth Controller', () => {
  let defaultUser = null;
  const password = '12345678';

  beforeAll(async () => {
    defaultUser = await User.create({
      name: faker.name.findName(),
      email: faker.internet.email(),
      password,
    });
  });

  describe('POST /auth/signin', () => {
    it('should return 400 with empty body', done => {
      request(app)
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({})
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 400 with empty password', done => {
      request(app)
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: 'luis@gmail.com',
          password: '',
        })
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return validation error messages with invalid data', done => {
      request(app)
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({})
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            done.fail(err);
          }

          expect(res.body.timestamp).toBeDefined();
          expect(res.body.error).toEqual('Bad request');
          expect(res.body.message).toBeDefined();
          expect(res.body.message.errors.length).not.toEqual(0);

          done();
        });
    });

    it('should return 200 OK and user token', done => {
      request(app)
        .post('/auth/signin')
        .set('Accept', 'application/json')
        .send({
          email: defaultUser.email,
          password,
        })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done.fail(err);
          }

          expect(res.body.token).toBeDefined();
          done();
        });
    });
  });

  describe('POST /auth/signup', () => {
    it('should return 400 with empty body', done => {
      request(app)
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({})
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 400 with weak password (min. length < 8)', done => {
      request(app)
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({
          email: 'luis@gmail.com',
          password: '123',
        })
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return validation error messages with invalid data', done => {
      request(app)
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({})
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            done.fail(err);
          }

          expect(res.body.timestamp).toBeDefined();
          expect(res.body.error).toEqual('Bad request');
          expect(res.body.message).toBeDefined();
          expect(res.body.message.errors.length).not.toEqual(0);

          done();
        });
    });

    it('should return success true when user is registered', done => {
      request(app)
        .post('/auth/signup')
        .set('Accept', 'application/json')
        .send({
          email: faker.internet.email(),
          password: faker.internet.password(),
        })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done.fail(err);
          }

          expect(res.body.success).toBeTruthy();
          done();
        });
    });
  });

  describe('POST /auth/validate-token', () => {
    it('should return 400 with invalid data', done => {
      request(app)
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({})
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 401 with invalid token', done => {
      request(app)
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({
          token: token.invalidToken,
        })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should return 200 and status equals to valid', done => {
      request(app)
        .post('/auth/validate-token')
        .set('Accept', 'application/json')
        .send({
          token: token.validToken,
        })
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done.fail(err);
          }

          expect(res.body.status).toEqual('valid');
          done();
        });
    });
  });
});
