const request = require('supertest');
const faker = require('faker');

const app = require('../../../app/index');

const { Book, sequelize } = require('../../../app/models');

const token = require('../../fixtures/token');

describe('Books Controller', () => {
  describe('As guest', () => {
    describe('GET /books', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .get('/books')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('GET /books/:isbn', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .get('/books/1')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('POST /books', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .post('/books')
          .send({})
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('PUT /books/:id', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .put('/books/1')
          .send({})
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('DELETE /books/:id', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .del('/books/1')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });
  });

  describe('As user with invalid token', () => {
    describe('GET /books', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .get('/books')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.invalidToken}`)
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('GET /books/:isbn', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .get('/books/1')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.invalidToken}`)
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('POST /books', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .post('/books')
          .send({})
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.invalidToken}`)
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('PUT /books/:id', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .put('/books/1')
          .send({})
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.invalidToken}`)
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });

    describe('DELETE /books/:id', () => {
      it('should return 401 unauthorized', done => {
        request(app)
          .del('/books/1')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.invalidToken}`)
          .expect('Content-Type', /json/)
          .expect(401, done);
      });
    });
  });

  describe('As valid authenticated user', () => {
    describe('GET /books', () => {
      it('should not accept page less than 1', done => {
        request(app)
          .get('/books?page=0')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(400, done);
      });

      it('should not accept per_page greather than 100', done => {
        request(app)
          .get('/books?per_page=125')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(400, done);
      });

      it('should return 200 OK and books data', done => {
        request(app)
          .get('/books?page=1&per_page=5')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if (err) {
              done.fail(err);
            }

            expect(res.body.data).toBeDefined();
            expect(res.body.data.length).toBeGreaterThan(0);

            done();
          });
      });
    });

    describe('GET /books/:isbn', () => {
      it('should return 404 with unknown isbn', done => {
        request(app)
          .get('/books/unknown')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(404, done);
      });

      it('should return book details', async done => {
        const book = await Book.findOne({
          order: sequelize.random(),
        });

        request(app)
          .get(`/books/${book.isbn}`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if (err) {
              done.fail(err);
            }

            expect(res.body).toBeDefined();
            expect(res.body.isbn).toEqual(book.isbn);
            expect(res.body.title).toEqual(book.title);
            expect(res.body.authors).toEqual(book.authors);
            expect(res.body.publisher).toEqual(book.publisher);
            expect(res.body.language).toEqual(book.language);
            expect(res.body.pages).toEqual(book.pages);

            done();
          });
      });
    });

    describe('POST /books', () => {
      it('should return 400 with empty body', done => {
        request(app)
          .post('/books')
          .send({})
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(400, done);
      });

      it('should return 400 with invalid parameters', done => {
        request(app)
          .post('/books')
          .send({ isbn: '', title: '', authors: '' })
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(400, done);
      });

      it('should create book register', done => {
        request(app)
          .post('/books')
          .send({
            isbn: faker.random.number(999999),
            title: faker.random.words(),
            authors: faker.name.findName(),
          })
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            if (err) {
              done.fail(err);
            }

            expect(res.body.success).toBeTruthy();
            done();
          });
      });
    });

    describe('PUT /books/:id', () => {
      it('should return 404 when book is not found', done => {
        request(app)
          .put('/books/1')
          .send({})
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(404, done);
      });

      it('should update book register', async done => {
        const book = await Book.findOne({
          order: sequelize.random(),
        });

        request(app)
          .put(`/books/${book.isbn}`)
          .send({ title: 'new', authors: 'new 2' })
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(200)
          .end(async (err, res) => {
            if (err) {
              done.fail(err);
            }

            const newBookData = await Book.findByPk(book.isbn);

            expect(res.body.success).toBeTruthy();
            expect(newBookData.title).toEqual('new');
            expect(newBookData.authors).toEqual('new 2');
            done();
          });
      });
    });

    describe('DELETE /books/:id', () => {
      it('should return 404 with unknown isbn', done => {
        request(app)
          .del('/books/unknown')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect('Content-Type', /json/)
          .expect(404, done);
      });

      it('should delete book', async done => {
        const book = await Book.findOne({
          order: sequelize.random(),
        });

        request(app)
          .del(`/books/${book.isbn}`)
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${token.validToken}`)
          .expect(204, done);
      });
    });
  });
});
