const faker = require('faker');

const { sequelize, Book } = require('../app/models');

const seedBooks = () => {
  return [...Array(10)].map(() => {
    return Book.create({
      isbn: faker.random.number(999999),
      title: faker.random.words(),
      authors: faker.name.findName(),
      publisher: faker.company.companyName(),
      pages: faker.random.number(999),
      short_description: faker.lorem.paragraph(),
    });
  });
};

beforeAll(async () => {
  await sequelize.sync();
  await Promise.all(seedBooks());
});
