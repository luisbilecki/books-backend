module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('books', {
      isbn: {
        type: Sequelize.STRING(13),
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: Sequelize.STRING(80),
        allowNull: false,
      },
      authors: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      short_description: {
        type: Sequelize.TEXT,
      },
      publisher: {
        type: Sequelize.STRING(60),
      },
      language: {
        type: Sequelize.STRING(20),
      },
      pages: {
        type: Sequelize.SMALLINT,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface /* , Sequelize */) => {
    return queryInterface.dropTable('Books');
  },
};
