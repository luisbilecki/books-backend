const JwtService = require('../services/jwt');
const DefaultError = require('../errors/defaultError');

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization');

  if (!authHeader) {
    return next(DefaultError.unAuthorized(req));
  }

  const token = authHeader.split(' ')[1];
  let decodedToken;

  try {
    decodedToken = JwtService.validate(token);
  } catch (err) {
    return next(DefaultError.unAuthorized(req));
  }

  if (!decodedToken) {
    return next(DefaultError.unAuthorized(req));
  }

  req.userEmail = decodedToken.email;

  next();
};
