const jwt = require('jsonwebtoken');

const { JWT_PRIVATE_KEY, JWT_PUBLIC_KEY } = process.env;

const signOptions = {
  issuer: 'Books backend',
  audience: 'books.lfbilecki.com',
  expiresIn: '1 days',
  algorithm: 'RS256',
};

const verifyOptions = {
  issuer: 'Books backend',
  audience: 'books.lfbilecki.com',
  algorithm: 'RS256',
};

const sign = email => {
  return jwt.sign(
    { email },
    JWT_PRIVATE_KEY.replace(/\\n/g, '\n'),
    signOptions
  );
};

const validate = token => {
  return jwt.verify(token, JWT_PUBLIC_KEY.replace(/\\n/g, '\n'), verifyOptions);
};

module.exports = {
  sign,
  validate,
};
