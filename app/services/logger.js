const { createLogger, format, transports } = require('winston');

const { combine, timestamp, printf } = format;

const myFormat = printf(data => {
  return `[${data.level.toUpperCase()}] ${data.timestamp}: ${data.message}`;
});

const logger = createLogger({
  format: combine(timestamp(), myFormat),
  transports: [new transports.Console()],
});

logger.stream = {
  write: message => {
    logger.info(message);
  },
};

module.exports = logger;
