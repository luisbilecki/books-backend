module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define(
    'Book',
    {
      isbn: {
        type: DataTypes.STRING(13),
        primaryKey: true,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      title: {
        type: DataTypes.STRING(80),
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      authors: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      short_description: {
        type: DataTypes.TEXT,
      },
      publisher: {
        type: DataTypes.STRING(60),
      },
      language: {
        type: DataTypes.STRING(20),
      },
      pages: {
        type: DataTypes.INTEGER,
      },
    },
    {
      underscored: true,
    }
  );

  Book.associate = (/* models */) => {};

  return Book;
};
