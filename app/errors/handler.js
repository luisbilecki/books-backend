const DefaultError = require('./defaultError');

const logger = require('../services/logger');

const handler = (err, req, res, next) => {
  // Check if is an instance of default error or return an 500 error
  const errorInstance =
    err instanceof DefaultError
      ? err
      : DefaultError.internalServerError(req, err);

  // 500 Internal Server Error
  const errData = errorInstance.message;
  if (errData) {
    logger.error(Object.values(errData).join('\n'));
  }

  res.status(errorInstance.status);
  res.json(errorInstance);
};

module.exports = handler;
