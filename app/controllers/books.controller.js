const { Book } = require('../models');

const DefaultError = require('../errors/defaultError');

// Pagination constants for Books
const PER_PAGE = 25;

const getBooks = async (req, res, next) => {
  const page = parseInt((req.query && req.query.page) || 1, 10);
  const size = parseInt((req.query && req.query.per_page) || PER_PAGE, 10);

  const offset = (page - 1) * size;
  const limit = size;

  const { rows, count } = await Book.findAndCountAll({
    order: [['updated_at', 'DESC']],
    limit,
    offset,
  });

  res.json({
    data: rows,
    current_page: page,
    total_count: count,
    per_page: size,
  });
};

const getBook = async (req, res, next) => {
  const { isbn } = req.params;

  const book = await Book.findByPk(isbn);

  if (!book) {
    return next(DefaultError.notFound(req));
  }

  res.json(book);
};

const createBook = async (req, res, next) => {
  const {
    isbn,
    title,
    authors,
    publisher,
    language,
    pages,
    shortDescription,
  } = req.body;

  const book = await Book.findByPk(isbn);
  if (book) {
    return next(DefaultError.unProcessableEntity(req, 'ISBN exists'));
  }

  await Book.create({
    isbn,
    title,
    authors,
    publisher,
    language,
    pages,
    short_description: shortDescription,
  });

  res.json({ success: true });
};

const updateBook = async (req, res, next) => {
  const { isbn } = req.params;
  const {
    title,
    authors,
    publisher,
    language,
    pages,
    shortDescription,
  } = req.body;

  const book = await Book.findByPk(isbn);

  if (!book) {
    return next(DefaultError.notFound(req));
  }

  await book.update({
    title,
    authors,
    publisher,
    language,
    pages,
    short_description: shortDescription,
  });

  res.json({ success: true });
};

const deleteBook = async (req, res, next) => {
  const { isbn } = req.params;
  const book = await Book.findByPk(isbn);

  if (!book) {
    return next(DefaultError.notFound(req));
  }

  await book.destroy();

  res.status(204).json();
};

module.exports = {
  getBooks,
  getBook,
  createBook,
  updateBook,
  deleteBook,
};
