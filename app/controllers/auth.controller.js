const { User } = require('../models');

const DefaultError = require('../errors/defaultError');

const JwtService = require('../services/jwt');

const signIn = async (req, res, next) => {
  const { email, password } = req.body;

  const user = await User.findOne({ where: { email } });

  if (!user) {
    return next(DefaultError.notFound(req));
  }

  const passwordMatches = user.comparePassword(password);

  if (!passwordMatches) {
    return next(DefaultError.unAuthorized(req, 'Wrong password!'));
  }

  const issuedToken = JwtService.sign(email);

  return res.json({ success: true, token: issuedToken });
};

const signUp = async (req, res, next) => {
  const { name, email, password } = req.body;

  // Password will be hashed using a beforeCreate hook in model
  await User.findOrCreate({
    where: { email },
    defaults: { name, email, password },
  });

  return res.json({ success: true });
};

const validateToken = async (req, res, next) => {
  const { token } = req.body;

  try {
    await JwtService.validate(token);

    res.json({ status: 'valid' });
  } catch (err) {
    return next(DefaultError.unAuthorized(req));
  }
};

module.exports = {
  signIn,
  signUp,
  validateToken,
};
