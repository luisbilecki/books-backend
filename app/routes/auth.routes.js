const express = require('express');

const { wrapAsync } = require('../utils/wrapFn');
const validatorMiddleware = require('../middlewares/requestValidator');

const authController = require('../controllers/auth.controller');

const signUpSchema = require('../validations/auth/signUp');
const signInSchema = require('../validations/auth/signIn');
const validateTokenSchema = require('../validations/auth/validateToken');

const router = express.Router();

router.post(
  '/signin',
  signInSchema,
  validatorMiddleware,
  wrapAsync(authController.signIn)
);

router.post(
  '/signup',
  signUpSchema,
  validatorMiddleware,
  wrapAsync(authController.signUp)
);

router.post(
  '/validate-token',
  validateTokenSchema,
  validatorMiddleware,
  authController.validateToken
);

module.exports = router;
