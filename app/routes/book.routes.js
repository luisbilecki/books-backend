const express = require('express');

const { wrapAsync } = require('../utils/wrapFn');

const isAuthMiddleware = require('../middlewares/isAuth');
const validatorMiddleware = require('../middlewares/requestValidator');

const booksController = require('../controllers/books.controller');

const getBooksSchema = require('../validations/books/getBooks');
const createBookSchema = require('../validations/books/createBook');
const updateBookSchema = require('../validations/books/updateBook');

const router = express.Router();

router.get(
  '/',
  isAuthMiddleware,
  getBooksSchema,
  validatorMiddleware,
  wrapAsync(booksController.getBooks)
);

router.get('/:isbn', isAuthMiddleware, wrapAsync(booksController.getBook));

router.post(
  '/',
  isAuthMiddleware,
  createBookSchema,
  validatorMiddleware,
  wrapAsync(booksController.createBook)
);

router.put(
  '/:isbn',
  isAuthMiddleware,
  updateBookSchema,
  validatorMiddleware,
  wrapAsync(booksController.updateBook)
);

router.delete(
  '/:isbn',
  isAuthMiddleware,
  wrapAsync(booksController.deleteBook)
);

module.exports = router;
