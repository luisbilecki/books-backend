const { body } = require('express-validator');

module.exports = [
  body('token')
    .not()
    .isEmpty(),
];
