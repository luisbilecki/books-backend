const { body, param } = require('express-validator');

module.exports = [
  param('isbn')
    .not()
    .isEmpty(),
  body('title')
    .isLength({ max: 80 })
    .not()
    .isEmpty()
    .optional(),
  body('authors')
    .not()
    .isEmpty()
    .optional(),
  body('shortDescription')
    .not()
    .isEmpty()
    .optional(),
  body('pages')
    .isInt({ min: 0 })
    .optional(),
  body('publisher')
    .not()
    .isEmpty()
    .optional(),
  body('language')
    .not()
    .isEmpty()
    .optional(),
];
