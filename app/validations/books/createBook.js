const { body } = require('express-validator');

module.exports = [
  body('isbn')
    .isLength({ max: 13 })
    .not()
    .isEmpty(),
  body('title')
    .isLength({ max: 80 })
    .not()
    .isEmpty(),
  body('authors')
    .not()
    .isEmpty(),
  body('shortDescription')
    .not()
    .isEmpty()
    .optional(),
  body('pages')
    .isInt({ min: 0 })
    .optional(),
  body('publisher')
    .not()
    .isEmpty()
    .optional(),
  body('language')
    .not()
    .isEmpty()
    .optional(),
];
