const { query } = require('express-validator');

module.exports = [
  query('page')
    .isInt({ min: 1 })
    .optional(),
  query('per_page')
    .isInt({ max: 100 })
    .optional(),
];
